#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_r5q.mk

COMMON_LUNCH_CHOICES := \
    lineage_r5q-user \
    lineage_r5q-userdebug \
    lineage_r5q-eng
